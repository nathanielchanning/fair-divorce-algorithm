# Nathaniel Channing
from random import sample 

# Generates a random permutation for you to test.
def get_permutation (n):
    return sample (xrange (n), n)

def get_valuation (n):
    husband = get_permutation (n)
    wife    = get_permutation (n)

    print "Husband: ", husband
    print "Wife: ", wife

    return (husband, wife)

# Calculates a fair partition if possible.
# It will return garbage if it isn't possible.
def fair_partition (valuations):
    #------------DEFINITIONS-------------
    # n shouldn't be odd.
    n = 10

    # define the leniencies.
    leniency_husband = 0
    leniency_wife    = 0

    # define the fair sets.
    fair_husband = []
    fair_wife    = []

    # define valuation sets.
    (husband, wife) = get_valuation (n)

    #------------ALGORITHM---------------
    while (not len (fair_wife) == n/2) and (not len (fair_husband) == n/2):
            # define the potential additions.
            husband_potential_additions = 0
            wife_potential_additions    = 0

            # get the potential additions for husband.
            for i in range (0, leniency_husband):
                if not husband [i] in fair_wife:
                    husband_potential_additions += 1
            
            # get the potential additions for wife.
            for i in range (0, leniency_wife):
                if not wife[i] in fair_husband:
                    wife_potential_additions += 1

            # if wife has less potential matches or
            # has the same but has less items in the respective
            # fair set.
            if ((wife_potential_additions < husband_potential_additions)
                or (wife_potential_additions == husband_potential_additions
                and len (fair_wife) < len (fair_husband))):
                # Keep going until an addition is found.
                current_item = wife. pop (0)

                while current_item in fair_husband:
                    leniency_wife -= 1
                    current_item = wife. pop (0)

                if not current_item == []:
                    fair_wife. append (current_item)
                    leniency_wife += 1

            # husband_potential_additions < wife_potential_additions    
            # or
            # (wife_potential_additions == husband_potential_additions
            # and len (fair_wife) > len (fair_husband))
            # Also the arbitrary complete tie case.
            else: 
                # Keep going until an addition is found.
                current_item = husband. pop (0)

                while current_item in fair_wife:
                    leniency_husband -= 1
                    current_item = husband. pop (0)

                if not current_item == []:
                    fair_husband. append (current_item)
                    leniency_husband += 1

    # Need to add to the set that isn't of size n/2
    # fair husband less than n/2
    if not len (fair_husband) == n/2:
        while not len (fair_husband) == n/2:
            current_item = husband. pop (0)

            if not current_item in fair_wife:
                fair_husband. append (current_item)

       
    # fair wife less than n/2
    else:
        while not len (fair_wife) == n/2:
            current_item = wife. pop (0)

            if not current_item in fair_wife:
                fair_wife. append (current_item)

    # Return the fair partition.
    return (fair_husband, fair_wife)

(husband, wife) = fair_partition (get_valuation)

print "Fair_Husband: ", husband
print "Fair_Wife: ",     wife
